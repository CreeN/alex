#include "Game.h"



Game::Game()
{
}

/******** INIT ********/

void Game::init()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		std::cerr << "SDL could not init! SDL Error: \n" << SDL_GetError() << std::endl;
	}
	else
	{
		m_gWindow = SDL_CreateWindow("MetaPsy", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 960, 640, SDL_WINDOW_SHOWN);

		if (m_gWindow == nullptr)
		{
			std::cerr << "Window could not create! SDL Error: \n" << SDL_GetError() << std::endl;
		}
		else
		{
			m_gSurface = SDL_GetWindowSurface(m_gWindow);

			/* BackGround Fill*/
			SDL_FillRect(m_gSurface, nullptr, (m_gSurface->format, 0x00, 0xFF, 0xFF));
			SDL_UpdateWindowSurface(m_gWindow);

		}
	}

	m_isRunning = true;
}

/******** DRAW ********/

void Game::draw()
{

}

/******** UPDATE ********/

void Game::update()
{

}

bool Game::isRunning() const
{
	return m_isRunning;
}

void Game::stop()
{
	SDL_DestroyWindow(m_gWindow);
	SDL_Quit();
}