#pragma once

#include <SDL.h>
#include <iostream>

class Game
{
public:
	Game();
	
	void init();
	void update();
	void draw();
	void stop();

	bool isRunning() const;

private:
	
	SDL_Window* m_gWindow;
	SDL_Surface* m_gSurface;

	bool m_isRunning;


};

